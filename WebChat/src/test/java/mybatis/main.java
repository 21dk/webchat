package mybatis;

import iv21dk.entity.AppRole;
import iv21dk.entity.AppUser;
//import iv21dk.entity.UserRole;
import iv21dk.entity.AppUserMessage;
import iv21dk.repository.RepositoryExeption;
import iv21dk.repository.specification.myBatis.AppUserMessageMyBatisSpecification;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import iv21dk.RepositoryConfiguration;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class main {

    SqlSessionFactory sqlSessionFactory;

    public static void main(String[] args) {

        JUnitCore runner = new JUnitCore();
        Result result = runner.run(main.class);
        System.out.println("run tests: " + result.getRunCount());
        System.out.println("failed tests: " + result.getFailureCount());
        System.out.println("ignored tests: " + result.getIgnoreCount());
        System.out.println("success: " + result.wasSuccessful());

    }

    @Before
    public void init() {

        sqlSessionFactory = new RepositoryConfiguration().sqlSessionFactory();
        /*if (!sqlSessionFactory.connect()) {
            System.out.println("Возникла ошибка при подключении к sql");
        }*/
    }

    @Test
    @Ignore
    public void testRole() {
        SqlSession session = sqlSessionFactory.openSession();

        AppRole appRole = session.selectOne("mybatis_mappers.RoleMapper.findById", 1);

        long user_id = 1;
        List <AppRole> roles = session.selectList("mybatis_mappers.RoleMapper.getUserRoles", user_id);

    }

    @Test
    @Ignore
    public void testUser() {

        SqlSession session = sqlSessionFactory.openSession();

        long id = 0;
        AppUser appUser = null;

        //insert
        /*AppRole roleUser = session.selectOne("mybatis_mappers.RoleMapper.findById", 2);

        List <AppRole> roles = new <AppRole>ArrayList();
        roles.add(roleUser);

        appUser = new AppUser("test test", "123", true);
        appUser.setUserRoles(roles);
        session.insert("mybatis_mappers.UserMapper.insert", appUser);
        session.insert("mybatis_mappers.UserMapper.insertUserRoles", appUser);
        session.commit();
        id = appUser.getId();*/

        if (id == 0){
            id = 8;
        }

        //select
        HashMap parameters = new HashMap();
        parameters.put("whereId", id);

        List<AppUser> list = session.selectList("mybatis_mappers.UserMapper.select", parameters);
        session.commit();

        appUser = null;
        if (list.size() != 0) {
            appUser = list.get(0);
        } else {
            System.out.println("Не удалось найти юзера с id = " + id);
        }

        //update
        /*if (appUser != null) {
            appUser.setUserName(appUser.getUserName() + "1");
            session.update("mybatis_mappers.UserMapper.update", appUser);
            session.commit();
        }*/
    }

    @Test
    public void selectMessage() throws RepositoryExeption {


        AppUserMessageMyBatisSpecification specification = new AppUserMessageMyBatisSpecification();
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<AppUserMessage> list = sqlSession.selectList("mybatis_mappers.UserMessageMapper.select", specification.getSpecification());
            sqlSession.commit();
            //return list;
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }


}