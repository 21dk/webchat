package iv21dk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
public class Application {
    public static void main(String[] args) throws Exception {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        /*System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }*/
    }

    //https://o7planning.org/ru/11705/create-a-login-application-with-spring-boot-spring-security-jpa
    //https://o7planning.org/ru/11543/create-a-login-application-with-spring-boot-spring-security-spring-jdbc#a13946811

}
