package iv21dk.transport;

import iv21dk.entity.AppUser;

public class TransportUser extends TransportObject {

    private long id;
    private String userName;
    private boolean enabled;

    public TransportUser(AppUser appUser) {
        this.id = appUser.getId();
        this.userName = appUser.getUserName();
        this.enabled = appUser.isEnabled();
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
