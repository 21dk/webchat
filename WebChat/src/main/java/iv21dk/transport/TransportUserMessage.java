package iv21dk.transport;

import iv21dk.entity.AppUserMessage;

import java.text.SimpleDateFormat;

public class TransportUserMessage extends TransportObject {
    private long id;
    private long userId;
    private String userName;
    private String date;
    private String text;

    public TransportUserMessage(AppUserMessage appUserMessage) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"); // ISO 8601

        this.id = appUserMessage.getId();
        this.userId = appUserMessage.getAppUser().getId();
        this.userName = appUserMessage.getAppUser().getUserName();
        this.date = dateFormat.format(appUserMessage.getDate());
        this.text = appUserMessage.getText();
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

}
