package iv21dk.transport;

public class TransportErrorMessage extends TransportObject {
    private String message;

    public TransportErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
