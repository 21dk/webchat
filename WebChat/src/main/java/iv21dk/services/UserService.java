package iv21dk.services;

import iv21dk.entity.AppRole;
import iv21dk.entity.AppUser;
import iv21dk.repository.AppUserRepository;
import iv21dk.repository.RepositoryExeption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        AppUser appUser;
        try {
            appUser = this.appUserRepository.findByUserName(userName);
        } catch (RepositoryExeption ex){
            ex.printStackTrace();
            return null;
        }

        if (appUser == null) {
            throw new UsernameNotFoundException("Пользователь " + userName + " не найден.");
        }

        // [ROLE_USER, ROLE_ADMIN,..]
        //List<String> roleNames = this.appRoleDAO.getRoleNames(appUser.getId());
        List <AppRole> userRoles = appUser.getUserRoles();
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (userRoles != null) {
            for (AppRole role : userRoles) {
                // ROLE_USER, ROLE_ADMIN,..
                GrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
                grantList.add(authority);
            }
        }

        UserDetails userDetails = (UserDetails) new User(appUser.getUserName(), //
                appUser.getEncrytedPassword(), grantList);

        return userDetails;
    }
}
