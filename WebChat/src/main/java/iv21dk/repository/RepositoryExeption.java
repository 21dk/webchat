package iv21dk.repository;

public class RepositoryExeption extends Exception {

    private String message;

    public RepositoryExeption(String message) {

        super(message);
        this.message = message;
    }
}
