package iv21dk.repository.myBatis;

import iv21dk.entity.AppUser;
import iv21dk.repository.AppUserRepository;
import iv21dk.repository.RepositoryExeption;
import iv21dk.repository.specification.myBatis.AppUserMyBatisSpecification;
import iv21dk.repository.specification.AppUserSpecification;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AppUserMyBatisRepository implements AppUserRepository {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void insert(AppUser appUser) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.insert("mybatis_mappers.UserMapper.insert", appUser);
            sqlSession.insert("mybatis_mappers.UserMapper.insertUserRoles", appUser);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }

    @Override
    public void update(AppUser appUser) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AppUser oldUser = findById(appUser.getId());
            sqlSession.update("mybatis_mappers.UserMapper.update", appUser);
            // Если роли не изменились, ничего не делаем
            if (appUser.getUserRoles().equals(oldUser.getUserRoles())){
                sqlSession.delete("mybatis_mappers.UserMapper.deleteUserRoles", appUser);
                sqlSession.insert("mybatis_mappers.UserMapper.insertUserRoles", appUser);
            }
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }

    @Override
    public void delete(AppUser appUser) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.delete("mybatis_mappers.UserMapper.deleteUserRoles", appUser);
            sqlSession.delete("mybatis_mappers.UserMapper.delete", appUser);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }

    @Override
    public AppUser findByUserName(String username) throws RepositoryExeption {
        AppUserMyBatisSpecification specification = new AppUserMyBatisSpecification();
        specification.findByUsername(username);
        List<AppUser> list = select(specification);
        if (list.size() != 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public AppUser findById(long id) throws RepositoryExeption {
        AppUserMyBatisSpecification specification = new AppUserMyBatisSpecification();
        specification.findById(id);
        List<AppUser> list = select(specification);
        if (list.size() != 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<AppUser> select(AppUserSpecification specification) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<AppUser> list = sqlSession.selectList("mybatis_mappers.UserMapper.select", specification.getSpecification());
            sqlSession.commit();
            return list;
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }
}
