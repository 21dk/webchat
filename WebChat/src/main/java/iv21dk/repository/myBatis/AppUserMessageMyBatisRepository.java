package iv21dk.repository.myBatis;

import iv21dk.entity.AppUserMessage;
import iv21dk.repository.AppUserMessageRepository;
import iv21dk.repository.RepositoryExeption;
import iv21dk.repository.specification.AppUserMessageSpecification;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AppUserMessageMyBatisRepository implements AppUserMessageRepository {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void insert(AppUserMessage appUserMessage) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.insert("mybatis_mappers.UserMessageMapper.insert", appUserMessage);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }

    @Override
    public void update(AppUserMessage appUserMessage) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.update("mybatis_mappers.UserMessageMapper.update", appUserMessage);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }

    @Override
    public void delete(AppUserMessage appUserMessage) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.delete("mybatis_mappers.UserMessageMapper.delete", appUserMessage);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }

    @Override
    public AppUserMessage findById(long id) throws RepositoryExeption {
        return null;
    }

    @Override
    public List<AppUserMessage> select(AppUserMessageSpecification specification) throws RepositoryExeption {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<AppUserMessage> list = sqlSession.selectList("mybatis_mappers.UserMessageMapper.select", specification.getSpecification());
            sqlSession.commit();
            return list;
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }
    }
}
