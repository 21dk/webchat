package iv21dk.repository.myBatis;

import iv21dk.entity.AppRole;
import iv21dk.repository.AppRoleRepository;
import iv21dk.repository.RepositoryExeption;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AppRoleMyBatisRepository implements AppRoleRepository {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public AppRole findById(int id) throws RepositoryExeption {
        AppRole appRole = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            appRole = sqlSession.selectOne("mybatis_mappers.RoleMapper.findById", id);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }

        if(appRole == null){
            throw new RepositoryExeption(String.format("Не найдена роль с id = %d", id));
        }
        return appRole;
    }

    @Override
    public AppRole findByName(String name) throws RepositoryExeption {
        AppRole appRole = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            appRole = sqlSession.selectOne("mybatis_mappers.RoleMapper.findByName", name);
            sqlSession.commit();
        } catch (Exception ex) {
            throw new RepositoryExeption(ex.getMessage());
        }

        if(appRole == null){
            throw new RepositoryExeption(String.format("Не найдена роль с именем = %s", name));
        }
        return appRole;
    }
}
