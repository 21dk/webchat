package iv21dk.repository;

import iv21dk.entity.AppRole;

public interface AppRoleRepository {
    AppRole findById(int id) throws RepositoryExeption;
    AppRole findByName(String name) throws RepositoryExeption;
}
