package iv21dk.repository;

import iv21dk.entity.AppUser;
import iv21dk.repository.specification.AppUserSpecification;

import java.util.List;

public interface AppUserRepository {

    void insert(AppUser appUser) throws RepositoryExeption;
    void update(AppUser appUser) throws RepositoryExeption;
    void delete(AppUser appUser) throws RepositoryExeption;

    AppUser findByUserName(String username) throws RepositoryExeption;
    AppUser findById(long id) throws RepositoryExeption;

    List <AppUser> select(AppUserSpecification specification) throws RepositoryExeption;
}
