package iv21dk.repository;

import iv21dk.entity.AppUserMessage;
import iv21dk.repository.specification.AppUserMessageSpecification;
import iv21dk.repository.specification.AppUserSpecification;

import java.util.List;

public interface AppUserMessageRepository {

    void insert(AppUserMessage appUserMessage) throws RepositoryExeption;
    void update(AppUserMessage appUserMessage) throws RepositoryExeption;
    void delete(AppUserMessage appUserMessage) throws RepositoryExeption;

    AppUserMessage findById(long id) throws RepositoryExeption;

    List<AppUserMessage> select(AppUserMessageSpecification specification) throws RepositoryExeption;

}
