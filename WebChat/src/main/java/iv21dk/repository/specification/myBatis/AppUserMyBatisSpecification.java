package iv21dk.repository.specification.myBatis;

import iv21dk.entity.AppUser;
import iv21dk.repository.specification.AppUserSpecification;

import java.util.HashMap;

public class AppUserMyBatisSpecification implements AppUserSpecification {

    private HashMap specification = new HashMap();

    @Override
    public boolean specified(AppUser appUser) {
        return true;
    }

    public void findById(long id) {
        specification.put("whereId", id);
    }

    public void findByUsername(String username) {
        specification.put("whereUsername", username);
    }

    public HashMap getSpecification() {
        return specification;
    }
}
