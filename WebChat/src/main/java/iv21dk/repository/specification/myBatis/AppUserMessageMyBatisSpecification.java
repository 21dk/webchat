package iv21dk.repository.specification.myBatis;

import iv21dk.repository.specification.AppUserMessageSpecification;

import java.util.HashMap;

public class AppUserMessageMyBatisSpecification implements AppUserMessageSpecification {

    private HashMap specification = new HashMap();

    @Override
    public HashMap getSpecification() {
        return specification;
    }

    public void findById(long id) {
        specification.put("whereId", id);
    }

    @Override
    public void Order(String order){
        specification.put("order", order);
    }

}
