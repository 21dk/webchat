package iv21dk.repository.specification;

import iv21dk.entity.AppUser;

import java.util.HashMap;

public interface AppUserSpecification {
    boolean specified(AppUser appUser);

    public HashMap getSpecification();
}
