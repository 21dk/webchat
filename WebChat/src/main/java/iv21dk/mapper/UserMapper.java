package iv21dk.mapper;

import iv21dk.entity.AppUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

// Вроде сейчас я не использую этот маппер. по моему представлению проваливаюсь непосредственно в xml из repository.
public interface UserMapper {

    void insert(AppUser appUser);
    void delete(AppUser appUser);
    void update(AppUser appUser);

    List<AppUser> select(HashMap parameters);
}
