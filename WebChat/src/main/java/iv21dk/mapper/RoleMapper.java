package iv21dk.mapper;

import iv21dk.entity.AppRole;

import java.util.List;

// Вроде сейчас я не использую этот маппер. по моему представлению проваливаюсь непосредственно в xml из repository.
public interface RoleMapper {
    AppRole findById(int id);

    //List<AppRole> getUserRoles(long user_id);
}
