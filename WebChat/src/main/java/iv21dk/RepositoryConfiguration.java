package iv21dk;

import iv21dk.repository.myBatis.AppRoleMyBatisRepository;
import iv21dk.repository.myBatis.AppUserMessageMyBatisRepository;
import iv21dk.repository.myBatis.AppUserMyBatisRepository;
import iv21dk.repository.specification.myBatis.AppUserMyBatisSpecification;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.Reader;

@Configuration
public class RepositoryConfiguration {

    public static final String REPOSITORI_ERROR = "Возникла ошибка на сервере, повторите попытку позже";

    @Bean
    public AppUserMyBatisRepository appUserRepository() {
        return new AppUserMyBatisRepository();
    }

    @Bean
    public AppRoleMyBatisRepository appRoleRepository() {
        return new AppRoleMyBatisRepository();
    }

    @Bean
    public AppUserMessageMyBatisRepository appUserMessageRepository() {
        return new AppUserMessageMyBatisRepository();
    }

    @Bean
    public AppUserMyBatisSpecification appUserMessageSpecification() {
        return new AppUserMyBatisSpecification();
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory(){
        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            return new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
