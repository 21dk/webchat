package iv21dk.entity;

import java.util.Date;

public class AppUserMessage {

    private Long id;
    private AppUser appUser;
    private Date date;
    private String text;

    public AppUserMessage() {
    }

    public AppUserMessage(Long id, AppUser appUser, Date date, String text) {
        this.id = id;
        this.appUser = appUser;
        this.date = date;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
