package iv21dk.utils;

import iv21dk.entity.AppUser;
import iv21dk.entity.AppUserMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageUtils {

    // старый метод передачи сообщения в виде готового html. сейчас передается объект.
    // html по аналогичному алгоритму готовится на клиенте
    public static String getMessageHtml(AppUserMessage appUserMessage) {
        String html = "";
        if (appUserMessage == null) {
            return html;
        }

        AppUser appUser = appUserMessage.getAppUser();
        Date date = appUserMessage.getDate();
        String text = appUserMessage.getText();

        String userBlok;
        if (appUser != null) {
            userBlok = String.format("<a href=\"userInfo\\%d\">%s</a> ", appUser.getId(), appUser.getUserName());
        } else {
            userBlok = "unknown";
        }

        String dateBlok = "";
        if (date != null) {

            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy"); // Для проверки тек даты. Знаю, можно иначе. пока не хочется заморачиваться

            Date currentDate = new Date();

            if (format.format(currentDate).equals(format.format(date))) {
                // Это тек. дата.
                format = new SimpleDateFormat("HH:mm");
            } else {
                format = new SimpleDateFormat("dd.MM.yyyy");
            }

            dateBlok = String.format("<i>%s:</i>", format.format(date));
        }

        if (text == null) {
            text = "";
        }

        return String.format("%s %s %s<br>", userBlok, dateBlok, text);
    }

}
