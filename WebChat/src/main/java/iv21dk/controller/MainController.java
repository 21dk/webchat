package iv21dk.controller;

import iv21dk.RepositoryConfiguration;
import iv21dk.entity.AppRole;
import iv21dk.entity.AppUser;
import iv21dk.transport.form.RegistrationForm;
import iv21dk.repository.AppRoleRepository;
import iv21dk.repository.AppUserRepository;
import iv21dk.repository.RepositoryExeption;
import iv21dk.utils.EncrytedPasswordUtils;
import iv21dk.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.ArrayList;

@Controller
public class MainController {

    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private AppRoleRepository appRoleRepository;

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String welcomePage(Model model) {
        model.addAttribute("title", "Добро пожаловать!");
        model.addAttribute("message", "Добро пожаловать на наш веб чат!");
        return "index";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(Model model, Principal principal) {

        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);

        return "adminPage";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model) {
        return "loginPage";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationPage(Model model) {
        return "registrationPage";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(Model model, //
                               @ModelAttribute("registrationForm") RegistrationForm registrationForm) {

        String username = registrationForm.getUsername();
        String password1 = registrationForm.getPassword1();
        String password2 = registrationForm.getPassword2();

        boolean error = false;
        if (username == null || username.isEmpty()){
            model.addAttribute("errorUsername", "Не заполнен логин");
            error = true;
        }
        if (password1 == null || password1.isEmpty()){
            model.addAttribute("errorPassword1", "Не заполнен пароль");
            error = true;
        }
        if (password2 == null || password2.isEmpty()){
            model.addAttribute("errorPassword2", "Не заполнен пароль");
            error = true;
        }
        if (!password1.equals(password2)){
            model.addAttribute("errorPassword2", "Введенные пароли не совпадают");
            error = true;
        }

        //AppUser appUser = this.appUserDAO.findUserAccount(username);
        AppUser appUser = null;
        try {
            appUser = appUserRepository.findByUserName(username);
        } catch (RepositoryExeption ex){
            ex.printStackTrace();
            error = true;
            model.addAttribute("errorMessage", RepositoryConfiguration.REPOSITORI_ERROR);
        }

        if (appUser != null) {
            model.addAttribute("errorUsername", "Логин уже используется");
            error = true;
        }

        if (!error) {
            appUser = new AppUser(username, EncrytedPasswordUtils.encrytePassword(password1), true);
            try {
                AppRole userRole = appRoleRepository.findByName("ROLE_USER");
                ArrayList<AppRole> roles = new ArrayList<AppRole>();
                roles.add(userRole);
                appUser.setUserRoles(roles);
                appUserRepository.insert(appUser);
            } catch (RepositoryExeption ex){
                ex.printStackTrace();
                error = true;
                model.addAttribute("errorMessage", RepositoryConfiguration.REPOSITORI_ERROR);
            }
        }

        if (error){
            // Научиться бы вообще не перезагружать форму.
            model.addAttribute("username", username);
            model.addAttribute("password1", password1);
            model.addAttribute("password2", password2);
            return "registrationPage";
        }

        //return "redirect:/registrationSuccessfulPage"; // Попробовать без redirect
        return "registrationSuccessfulPage";
    }

    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("title", "Logout");
        return "logoutSuccessfulPage";
    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public String userInfo(Model model, Principal principal) {

        // After user login successfully.
        String userName = principal.getName();

        //System.out.println("User Name: " + userName);

        //User loginedUser = (User) ((Authentication) principal).getPrincipal();

        //String userInfo = WebUtils.toString(loginedUser);
        //model.addAttribute("userInfo", userInfo);

        model.addAttribute("username", userName);

        return "userInfoPage";
    }

    @RequestMapping(value = "/userInfo/{id}", method = RequestMethod.GET)
    public String userInfoById(@PathVariable("id") long id, Model model, Principal principal) {

        AppUser appUser;
        try{
            appUser = this.appUserRepository.findById(id);

            model.addAttribute("username", appUser.getUserName());

        } catch (RepositoryExeption ex){
            ex.printStackTrace();
            model.addAttribute("errorMessage", RepositoryConfiguration.REPOSITORI_ERROR);
        }

        return "userInfoPage";
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(Model model, Principal principal) {

        if (principal != null) {
            User loginedUser = (User) ((Authentication) principal).getPrincipal();

            String userInfo = WebUtils.toString(loginedUser);

            model.addAttribute("userInfo", userInfo);

            String message = String.format("Привет %s.<br> У вас нет доступа к этой странице!", principal.getName());
            model.addAttribute("message", message);

        }

        return "403Page";
    }
}
