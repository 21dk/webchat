package iv21dk.controller;

import iv21dk.RepositoryConfiguration;
import iv21dk.entity.AppUser;
import iv21dk.entity.AppUserMessage;
import iv21dk.transport.TransportObject;
import iv21dk.transport.form.FormOfSend;
import iv21dk.repository.AppUserMessageRepository;
import iv21dk.repository.AppUserRepository;
import iv21dk.repository.RepositoryExeption;
import iv21dk.repository.specification.AppUserMessageSpecification;
import iv21dk.repository.specification.myBatis.AppUserMessageMyBatisSpecification;
import iv21dk.transport.TransportErrorMessage;
import iv21dk.transport.TransportUserMessage;
import iv21dk.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ChatController {

    @Autowired
    private AppUserMessageRepository appUserMessageRepository;
    //@Autowired
    //private AppUserMessageSpecification appUserMessageSpecification;
    @Autowired
    private AppUserRepository appUserRepository;

    @RequestMapping(value = {"/chat"}, method = RequestMethod.GET)
    public String chat(Model model) {
        return "chat";
    }

    @RequestMapping(value = {"/OldMessages"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TransportObject> getOldMessages() {

        List<TransportObject> transportMessages = new ArrayList<TransportObject>();

        try {
            AppUserMessageSpecification specification = new AppUserMessageMyBatisSpecification();
            specification.Order("date DESC");
            List<AppUserMessage> list = appUserMessageRepository.select(specification); // временно

            for (AppUserMessage appUserMessage : list) {
                transportMessages.add(new TransportUserMessage(appUserMessage));
            }

        } catch (RepositoryExeption ex) {
            transportMessages.add(new TransportErrorMessage(RepositoryConfiguration.REPOSITORI_ERROR));
        }

        return transportMessages;
    }

    // старый вариант отправки на сервер сообщения. заменен веб-сокетом.
    @Deprecated
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public String sendMessage(Model model, Principal principal,
                              @ModelAttribute("formOfSend") FormOfSend formOfSend) {

        String message = formOfSend.getMessage();

        if (message == null || message.isEmpty()) {
            model.addAttribute("messageError", "Не заполнено сообщение");
            return "chat";
        }

        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        try {
            AppUser appUser = appUserRepository.findByUserName(loginedUser.getUsername());

            AppUserMessage appUserMessage = new AppUserMessage();
            appUserMessage.setDate(new Date());
            appUserMessage.setAppUser(appUser);
            appUserMessage.setText(message);

            appUserMessageRepository.insert(appUserMessage);

        } catch (RepositoryExeption ex) {
            ex.printStackTrace();
            //error = true;
            model.addAttribute("errorMessage", RepositoryConfiguration.REPOSITORI_ERROR);
        }

        //return "redirect:/registrationSuccessfulPage"; // Попробовать без redirect
        addAttributeHtmlMessages(model);
        return "chat";
    }

    @Deprecated
    private void addAttributeHtmlMessages(Model model) {

        try {
            //List<AppUserMessage> list = appUserMessageRepository.select(appUserMessageSpecification);
            AppUserMessageSpecification specification = new AppUserMessageMyBatisSpecification();
            specification.Order("date DESC");
            List<AppUserMessage> list = appUserMessageRepository.select(specification); // временно

            List<String> htmlMessages = new ArrayList<String>();

            for (AppUserMessage appUserMessage : list) {
                htmlMessages.add(MessageUtils.getMessageHtml(appUserMessage));
            }

            //return htmlMessages;
            model.addAttribute("messages", htmlMessages);
        } catch (RepositoryExeption ex) {
            //return null;
            model.addAttribute("errorMessage", RepositoryConfiguration.REPOSITORI_ERROR);
        }

    }

    // WebSocket
    @MessageMapping("/send-message")
    @SendTo("/topic/main-chat")
    private TransportObject sendMessageByWebSocket(Principal principal, FormOfSend formOfSend) throws Exception {

        String message = formOfSend.getMessage();

        if (message == null || message.isEmpty()) {
            return new TransportErrorMessage("Не заполнено сообщение");
        }

        try {
            AppUser appUser = appUserRepository.findByUserName(principal.getName());

            AppUserMessage appUserMessage = new AppUserMessage();
            appUserMessage.setDate(new Date());
            appUserMessage.setAppUser(appUser);
            appUserMessage.setText(message);

            appUserMessageRepository.insert(appUserMessage);

            return new TransportUserMessage(appUserMessage);

        } catch (RepositoryExeption ex) {
            ex.printStackTrace();
            return new TransportErrorMessage(RepositoryConfiguration.REPOSITORI_ERROR);
        }
    }


}
