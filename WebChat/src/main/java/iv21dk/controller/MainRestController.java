package iv21dk.controller;


import iv21dk.entity.AppUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

// Пример рест контроллера.

//@Controller - не знаю чем отличается. работает так же. возвращает json
@RestController
@RequestMapping("/api")
public class MainRestController {

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<?> getUser() {

        AppUser appUser = new AppUser( "ivan", "123", true);

        return new ResponseEntity<AppUser>(appUser, HttpStatus.OK);
    }

    // url должен выглядеть так .../api/user/12
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("id") long id) {

        AppUser appUser = new AppUser(id, "ivan", "123");

        return new ResponseEntity<AppUser>(appUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/userTxt", method = RequestMethod.GET)
    public String getUserTxt() {

        //AppUser appUser = new AppUser( "ivan", "123", true);

        //return new ResponseEntity<AppUser>(appUser, HttpStatus.OK);
        return "Spring говорит ДА!";
    }


}
