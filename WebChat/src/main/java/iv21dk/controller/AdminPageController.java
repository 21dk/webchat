package iv21dk.controller;

import iv21dk.RepositoryConfiguration;
import iv21dk.entity.AppUser;
import iv21dk.entity.AppUserMessage;
import iv21dk.repository.AppUserMessageRepository;
import iv21dk.repository.AppUserRepository;
import iv21dk.repository.RepositoryExeption;
import iv21dk.repository.specification.AppUserMessageSpecification;
import iv21dk.repository.specification.AppUserSpecification;
import iv21dk.repository.specification.myBatis.AppUserMessageMyBatisSpecification;
import iv21dk.repository.specification.myBatis.AppUserMyBatisSpecification;
import iv21dk.transport.TransportErrorMessage;
import iv21dk.transport.TransportObject;
import iv21dk.transport.TransportUser;
import iv21dk.transport.TransportUserMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/adminPage")
public class AdminPageController {

    @Autowired
    private AppUserMessageRepository appUserMessageRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @RequestMapping(value = {"/messages"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TransportObject> getMessages() {

        List<TransportObject> transportList = new ArrayList<TransportObject>();

        try {
            AppUserMessageSpecification specification = new AppUserMessageMyBatisSpecification();
            specification.Order("date");
            List<AppUserMessage> list = appUserMessageRepository.select(specification); // временно

            for (AppUserMessage appUserMessage : list) {
                transportList.add(new TransportUserMessage(appUserMessage));
            }

        } catch (RepositoryExeption ex) {
            transportList.add(new TransportErrorMessage(RepositoryConfiguration.REPOSITORI_ERROR));
        }

        return transportList;
    }

    @RequestMapping(value = {"/users"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TransportObject> getUsers() {

        List<TransportObject> transportList = new ArrayList<TransportObject>();

        try {
            AppUserSpecification specification = new AppUserMyBatisSpecification();
            List<AppUser> list = appUserRepository.select(specification);

            for (AppUser appUser : list) {
                transportList.add(new TransportUser(appUser));
            }

        } catch (RepositoryExeption ex) {
            transportList.add(new TransportErrorMessage(RepositoryConfiguration.REPOSITORI_ERROR));
        }

        return transportList;
    }


}
