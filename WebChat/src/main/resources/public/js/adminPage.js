
function loadPage(){
    users();
}

function users(){
    $("#a_users").addClass("primary");
    $("#a_messages").removeClass("primary");

    $("#table_users").css('display', 'block');
    $("#table_messages").css('display', 'none');

    $.get(
                '/adminPage/users',
                function(data) {
                    receivedUsers(data);
                },
                'json');

}

function messages(){
    $("#a_users").removeClass("primary");
    $("#a_messages").addClass("primary");

    $("#table_users").css('display', 'none');
    $("#table_messages").css('display', 'block');
}

function receivedUsers(data){

    for(let objectUser of data){
        //receivedInputMessage(messageObject, false);

        var hasSimpleTypeProp = "simpleType" in objectUser;
        if (!hasSimpleTypeProp){
            console.log("Неверный формат входящего сообщения. Сообщение должно иметь свойство simpleType");
            return;
        }

        switch(objectUser.simpleType){
            case "TransportUser":
                addUserInTable(objectUser);

                break;
            case "TransportErrorMessage":
               showErrorMessage(messageObject);
               break;
            default:
               console.log(messageObject.simpleType + " - неизвестный тип входящего сообщения");
               break;
        }

    }

}

dateTimeReviver = function dateTimeReviver(key, value) {
    var a;
    if (key == "date" && typeof value === 'string') {
        return new Date(value);
    }
    return value;
}

function showErrorMessage(messageObject){
    elemErrorMessage = $("#errorMessage");
    elemErrorMessage.text(messageObject.message);
    elemErrorMessage.css('display', 'block');
}

function addUserInTable(objectUser){

    var tr = document.createElement('tr');

    var td = document.createElement('td');
    td.innerText = objectUser.id;
    tr.append(td);

    var td = document.createElement('td');
    td.innerText = objectUser.userName;
    tr.append(td);

    var td = document.createElement('td');
    td.innerText = objectUser.enabled;
    tr.append(td);

    $("#table_users").append(tr);

}
