var stompClient = null;

function setConnected(connected) {
    //document.getElementById('connect').disabled = connected;
    //document.getElementById('disconnect').disabled = !connected;
    //document.getElementById('conversationDiv').style.visibility = connected ? 'visible' : 'hidden';
    //document.getElementById('response').innerHTML = '';
}

function connect() {
    var socket = new SockJS('/main-chat');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        setConnected(true);
        stompClient.subscribe('/topic/main-chat', function(inputMessage){
            receivedInputResponseText(inputMessage.body);
        });
    });
}

function disconnect() {
    stompClient.disconnect();
    setConnected(false);
    //console.log("Disconnected");
}

function loadPage(){
    // после подключения могут приходить сообщения еще до загрузки истории.
    // новые сообщения вставляются наверх.
    // исторические вставляются вниз. Таким образом сохранится порядок сообщений.
    connect();
    getOldMessages();
//    var messages = "${messages}";
//    console.log(messages);
}

function getOldMessages(){
    $.get(
        '/OldMessages',
        function(data) {
            receivedOldMessages(data);
        },
        'json');
}

function sendMessage() {
    var messageText = $("#message").val();
    stompClient.send("/app/send-message", {}, JSON.stringify({'message': messageText}));
    $("#message").val("");

    $("#errorMessage").css('display', 'none');
}

function receivedInputResponseText(responseText) {
    var messageObject = JSON.parse(responseText, dateTimeReviver);
    receivedInputMessage(messageObject, true);
}

// старый вариант через стандартный ajax
function receivedOldMessages1(responseText) {
    var messagesObjects = JSON.parse(responseText, dateTimeReviver);
    // старые сообщения размещаем вниз. приходят они отсортированные по убыванию.
    for(let messageObject of messagesObjects){
        receivedInputMessage(messageObject, false);
    }
}

function receivedOldMessages(data) {
    // старые сообщения размещаем вниз. приходят они отсортированные по убыванию.
    for(let messageObject of data){
        messageObject.date = dateTimeReviver("date", messageObject.date);
        receivedInputMessage(messageObject, false);
    }
}

function receivedInputMessage(messageObject, onTop) {

    var hasSimpleTypeProp = "simpleType" in messageObject;
    if (!hasSimpleTypeProp){
        console.log("Неверный формат входящего сообщения. Сообщение должно иметь свойство simpleType");
        return;
    }

    switch(messageObject.simpleType){
        case "TransportUserMessage":
            showUserMessage(messageObject, onTop);
            break;
        case "TransportErrorMessage":
           showErrorMessage(messageObject);
           break;
        default:
           console.log(messageObject.simpleType + " - неизвестный тип входящего сообщения");
           break;
    }
}

function showUserMessage(messageObject, onTop){

    var div = getHtmlMessage(messageObject);

    if(onTop){
        $("#chatlist").prepend(div);
    } else {
        $("#chatlist").append(div);
    }
}

function showErrorMessage(messageObject){
    elemErrorMessage = $("#errorMessage");
    elemErrorMessage.text(messageObject.message);
    elemErrorMessage.css('display', 'block');
}

dateTimeReviver = function dateTimeReviver(key, value) {
    var a;
    if (key == "date" && typeof value === 'string') {
        return new Date(value);
    }
    return value;
}

function getHtmlMessage(messageObject){

    var userBlock = `<a href=\"userInfo\\${messageObject.userId}">${messageObject.userName}</a>`;

    var dateBlock = "";
	var fullDate = messageObject.date;
	if (fullDate instanceof Date) {
		var currentFullDate = new Date();
		var currentDate = new Date(currentFullDate.getFullYear(), currentFullDate.getMonth(), currentFullDate.getDate());

		var date = new Date(fullDate.getFullYear(), fullDate.getMonth(), fullDate.getDate());

		// форматируем дату. явно есть более изящный способ
		if (currentDate.getTime()===date.getTime()){
			//dateBlock = fullDate.format("mm.dd.yyyy");
			dateBlock = "" + addZeros(fullDate.getHours(), 2) + ":" + addZeros(fullDate.getMinutes(), 2);
		} else {
			//dateBlock = fullDate.format("mm.dd.yyyy");
			dateBlock = "" + addZeros(fullDate.getDate(), 2) + "." + addZeros((fullDate.getMonth() + 1),2) + "." + fullDate.getFullYear();
		}

		dateBlock = ` <i>${dateBlock}:</i>`;

	}

    var messageWrapper = document.createElement('div');

    var div = document.createElement('div');
    div.innerHTML = `${userBlock}${dateBlock} `;
    messageWrapper.append(div);

    var div = document.createElement('div');
    div.innerText = messageObject.text;
    messageWrapper.append(div);

    messageWrapper.append(document.createElement('br'));

    return messageWrapper;
}

function addZeros(n, needLength) {
  needLength = needLength || 2;
  n = String(n);
  while (n.length < needLength) {
    n = "0" + n;
  }
  return n
}


